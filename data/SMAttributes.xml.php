<?php exit(); ?>
<?xml version="1.0" encoding="ISO-8859-1"?>
<database>
	<entry key="SMGalleryColumns" value="1"/>
	<entry key="SMGalleryRows" value="2"/>
	<entry key="SMGalleryWidth" value=""/>
	<entry key="SMGalleryHeight" value="180"/>
	<entry key="SMGalleryPadding" value="3"/>
	<entry key="SMContactRecipients" value="hardtux@zohomail.com"/>
	<entry key="SMContactSubject" value="Request from Website"/>
	<entry key="SMContactSuccessMessage" value="Thanks, we will get back to you shortly"/>
	<entry key="SMImageMontageMinHeight" value="100"/>
	<entry key="SMImageMontageMaxHeight" value="300"/>
	<entry key="SMImageMontageMargin" value="0"/>
	<entry key="SMImageMontageDisplayTitle" value="true"/>
	<entry key="SMImageMontageDisplayImageTitle" value="false"/>
	<entry key="SMImageMontageDisplayImageExif" value="false"/>
	<entry key="SMImageMontageDisplayPicker" value="false"/>
	<entry key="SMImageMontageShuffle" value="true"/>
	<entry key="SMImageMontageSlideShowInterval" value="3000"/>
	<entry key="SMEnvironmentSessionName/" value="SMSESSION8394216b2efe8179"/>
	<entry key="SMPagesAutoSeoUrlsVersion" value="20160828"/>
	<entry key="SMAutoSeoUrlsCompleted" value="true"/>
	<entry key="SMPagesSettingsUrlType" value="Filename"/>
	<entry key="SMPagesSettingsSeoUrls" value="true"/>
	<entry key="SMClientCacheKey" value="db8ae19b36197b02748f1fe64e9b5c55"/>
	<entry key="SMContactCONF1_Recipients" value="hardtux@zohomail.com"/>
	<entry key="SMContactCONF1_Subject" value="Quiz"/>
	<entry key="SMContactCONF1_SuccessMessage" value="Thank you for your inquiry, we will get back to you on the specified e-mail as soon as possible."/>
</database>